import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users:Array<User>=[];
  Msgstore:string=""
  Msgdelete:string="";
  Msgupdate:string="";
  flag:boolean = false;
  id:number=0;
  name:string = "";
  
  constructor(public pser1:UserService) { }   // DI for Service class

  // it is a life cycle or hook of component it will call after constructor. 
  // it call only one time. 
  ngOnInit(): void {
    this.loadUsers();
    
  }

  loadUsers(): void{
    //console.log("Event fired")
    this.pser1.loadUserDetails().subscribe(res=>this.users=res);
  }

  storeUser(userRef:NgForm){
    //console.log(productRef.value);  
    this.pser1.storeUserDetails(userRef.value).
    subscribe(res=>this.Msgstore=res,error=>console.log(error),()=>this.loadUsers());  
  }

  deleteUser(id:number){
    //console.log(pid);
    this.pser1.deleteUserDetails(id).
    subscribe(res=>this.Msgdelete=res,error=>console.log(error),()=>this.loadUsers())
  }

  updateUser(user:User){
    //console.log(product);
    this.flag=true;
    this.id=user.id;
    this.name=user.name;
  }

  updateUserName() {
    let user ={"id":this.id,"name":this.name}
    //console.log(book);
    this.pser1.updateUserInfo(user).subscribe(result=>this.Msgupdate=result,
    error=>console.log(error),
    ()=>{
    this.loadUsers();
    this.flag=false;  
    })
  }
}
