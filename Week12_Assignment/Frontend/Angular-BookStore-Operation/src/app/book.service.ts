import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from './book';
@Injectable({
  providedIn: 'root' // giving the information in providers attribute in app.module.ts file 
})
export class BookService {

  constructor(public http:HttpClient) { }   // DI for HttpClient API

  // loadProductDetails() {
  //   console.log("service called..")
  //   this.http.get("http://localhost:9090/product/allProducts").
  //   subscribe(res=>console.log(res),error=>console.log(error),()=>console.log("completed"));
  // }

  loadBookDetails():Observable<Book[]> {
    return this.http.get<Book[]>("http://localhost:9090/book/getAllBook")
  }

  storeBookDetails(book:Book):Observable<string>{
    return this.http.post("http://localhost:9090/book/storeBook",book,{responseType:'text'})
  }  

  deleteBookDetails(id:number):Observable<string>{
    return this.http.delete("http://localhost:9090/book/deleteBook/"+id,{responseType:'text'});
  }

  updateBookInfo(book:any):Observable<string>{
    return this.http.patch("http://localhost:9090/book/updateBook",book,{responseType:'text'})
  }
  
}

