import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Book } from '../book';
import { BookService } from '../book.service';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userName:string="";
  books:Array<Book>=[];
  storeMsg:string=""
  deleteMsg:string="";
  updateMsg:string="";
  flag:boolean = false;
  id:number=0;
  category:string = "";

  users:Array<User>=[];
  Msgstore:string=""
  Msgdelete:string="";
  Msgupdate:string="";
  //flag:boolean = false;
  //id:number=0;
  name:string = "";
  
  constructor(public router:Router,public pser:BookService,public pser1:UserService) { }   // DI for Service class

  // it is a life cycle or hook of component it will call after constructor. 
  // it call only one time. 
  ngOnInit(): void {
    let obj = sessionStorage.getItem("uname");
    if(obj!=null){
      this.userName=obj;
    }
    this.loadBooks();
    this.loadUsers();
    
  
  }

  loadBooks(): void{
    //console.log("Event fired")
    this.pser.loadBookDetails().subscribe(res=>this.books=res);
  }

   storeBook(bookRef:NgForm){
    //console.log(productRef.value);  
    this.pser.storeBookDetails(bookRef.value).
    subscribe(res=>this.storeMsg=res,error=>console.log(error),()=>this.loadBooks());  
  }

  
  deleteBook(id:number){
    //console.log(pid);
    this.pser.deleteBookDetails(id).
    subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadBooks())
  }

  

  updateBook(book:Book){
    //console.log(product);
    this.flag=true;
    this.id=book.id;
    this.category=book.category;
  }


  updateBookCategory() {
    let book ={"id":this.id,"category":this.category}
    //console.log(book);
    this.pser.updateBookInfo(book).subscribe(result=>this.updateMsg=result,
    error=>console.log(error),
    ()=>{
    this.loadBooks();
    this.flag=false;  
    })
  
  }

  loadUsers(): void{
    //console.log("Event fired")
    this.pser1.loadUserDetails().subscribe(res=>this.users=res);
  }

  storeUser(userRef:NgForm){
    //console.log(productRef.value);  
    this.pser1.storeUserDetails(userRef.value).
    subscribe(res=>this.Msgstore=res,error=>console.log(error),()=>this.loadUsers());  
  }

  deleteUser(id:number){
    //console.log(pid);
    this.pser1.deleteUserDetails(id).
    subscribe(res=>this.Msgdelete=res,error=>console.log(error),()=>this.loadUsers())
  }

  updateUser(user:User){
    //console.log(product);
    this.flag=true;
    this.id=user.id;
    this.name=user.name;
  }

  updateUserName() {
    let user ={"id":this.id,"name":this.name}
    //console.log(book);
    this.pser1.updateUserInfo(user).subscribe(result=>this.Msgupdate=result,
    error=>console.log(error),
    ()=>{
    this.loadUsers();
    this.flag=false;  
    })
  }

  logout() {
      sessionStorage.removeItem("uname");
      this.router.navigate(["login"]);
  }


  
}

  




  
 