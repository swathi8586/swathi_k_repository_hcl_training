import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {

  userloginRef = new FormGroup({
    user:new FormControl(),
    pass:new FormControl()
  });
  msg:string=""
  
    constructor(public router:Router) { }     // do DI for Router API 
    ngOnInit(): void {
    }
    checkUser() {
      let login = this.userloginRef.value;
      // we write this code inside a subscribe method 
      if(login.user=="Ajay" && login.pass=="123"){
          console.log("success");
          sessionStorage.setItem("uname",login.user);
          this.router.navigate(["home"]);
      }else {
          this.msg = "Invalid username or password"
          console.log("failure");
      }
      this.userloginRef.reset();
    }
    
  }


