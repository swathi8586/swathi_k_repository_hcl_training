import { Text } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Book } from '../book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  books:Array<Book>=[];
  storeMsg:string=""
  deleteMsg:string="";
  updateMsg:string="";
  flag:boolean = false;
  id:number=0;
  category:string = "";
  
  constructor(public pser:BookService) { }   // DI for Service class

  // it is a life cycle or hook of component it will call after constructor. 
  // it call only one time. 
  ngOnInit(): void {
    this.loadBooks();
    
  }

  loadBooks(): void{
    //console.log("Event fired")
    this.pser.loadBookDetails().subscribe(res=>this.books=res);
  }

  storeBook(bookRef:NgForm){
    //console.log(productRef.value);  
    this.pser.storeBookDetails(bookRef.value).
    subscribe(res=>this.storeMsg=res,error=>console.log(error),()=>this.loadBooks());  
  }

  deleteBook(id:number){
    //console.log(pid);
    this.pser.deleteBookDetails(id).
    subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadBooks())
  }

  updateBook(book:Book){
    //console.log(product);
    this.flag=true;
    this.id=book.id;
    this.category=book.category;
  }

  updateBookCategory() {
    let book ={"id":this.id,"category":this.category}
    //console.log(book);
    this.pser.updateBookInfo(book).subscribe(result=>this.updateMsg=result,
    error=>console.log(error),
    ()=>{
    this.loadBooks();
    this.flag=false;  
    })
  }
}
