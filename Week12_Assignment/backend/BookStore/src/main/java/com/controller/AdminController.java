package com.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Admin;
import com.service.AdminService;

@RestController
@RequestMapping("/admin")
@EntityScan(basePackages = "com.bean")
public class AdminController {
	
	@Autowired
	AdminService adminservice;

	@PostMapping(value ="AddAdmin",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String insertAdmin(@RequestBody Admin admin)
	{
		return adminservice.addAdminInfo(admin);
	}

	@PostMapping(value = "login",consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.TEXT_PLAIN_VALUE)
	public String adminDetails(@RequestBody Admin admin)
	{
		Admin find =  adminservice.login(admin.getEmail(),admin.getPassword());
		if(Objects.nonNull(find))
		{
			return "login successg=fully";
		}
		else {
			return "login failed";
		}
	}

	@PostMapping(value = "logout",consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.TEXT_PLAIN_VALUE)
	public String adminLogout(HttpSession hs)
	{
		hs.invalidate();
		return "logout successfully";
	}
	}

