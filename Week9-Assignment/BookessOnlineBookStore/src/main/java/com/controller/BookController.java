package com.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Book;
import com.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@GetMapping(value = "getAllBook",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<Book> getAllProductInfo() {
				return bookService.getAllbook();
			}

	@PostMapping(value = "storeBook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBookInfo(@RequestBody Book book) {
		
				return bookService.storeBookInfo(book);
	}
	
	
	@DeleteMapping(value = "deleteBook/{id}")
	public String deleteBookInfo(@PathVariable("id") int id) {
					return bookService.deleteBookInfo(id);
	}
	
	@PatchMapping(value = "updateBook")
	public String updateBookInfo(@RequestBody Book book) {
					return bookService.updateBookInfo(book);
	}

	
	
	
	
	
	
	
	
	


	

}
