create database books_Swathi_k;
use books_Swathi_k;

create table user(id int primary key,name varchar(100),email varchar(100),password varchar(100));
create table book(id int,name varchar(500),category varchar(200));
create table admin(name varchar(100),email varchar(100),password varchar(100)); 

insert into user values(1,"ajay","a@gmail.com","123");
insert into user values(2,"vijay","v@gmail.com","234");
insert into user values(3,"divya","d@gmail.com","456");


insert into book values( 1,"Invisible Man","Novel");
insert into book values( 2,"Scotland street","Comic");
insert into book values( 3,"Culinary Arts","cooking");


insert into admin values(1,"admin","admin@gmail.com","123");
insert into admin values(2,"admin2","admin2@gmail.com","134");

select * from admin;

select * from user;

select * from book;