package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;

@Service

public class AdminService {
	@Autowired
	AdminDao admindao;
	
	
public Admin login(String email,String password) {
		Admin admin = admindao.findByEmailAndPassword(email,password);
		return admin;
		
	}
	public String addAdminInfo(Admin admin) {
		if(admindao.existsById(admin.getEmail()))
		{
			return "This mail id already present";
		}
		else {
			admindao.save(admin);
			return"admin saved successfully";
		}
	}
	

}
