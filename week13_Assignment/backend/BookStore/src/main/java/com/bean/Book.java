package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book {
@Id

private int id;
private String name;
private String category;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
@Override
public String toString() {
	return "Book [id=" + id + ", name=" + name + ", category=" + category + "]";
}



}
