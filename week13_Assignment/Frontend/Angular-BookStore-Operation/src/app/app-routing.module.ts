import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DisplaybookComponent } from './displaybook/displaybook.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserloginComponent } from './userlogin/userlogin.component';

const routes: Routes = [

  {
    path:'userlogin',component:UserloginComponent
  },
{
    path:'displaybook',component:DisplaybookComponent
  },
  
{
  path:'login',component:LoginComponent
},

{
  path:'register',component:RegisterComponent
},

{
  path:'home',component:DashboardComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
