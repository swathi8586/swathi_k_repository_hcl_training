import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-displaybook',
  templateUrl: './displaybook.component.html',
  styleUrls: ['./displaybook.component.css']
})
export class DisplaybookComponent implements OnInit {
  books:Array<Book>=[];
 
  constructor(public pser:BookService) { }

  ngOnInit(): void {
    this.loadBooks();
  }
  loadBooks() {
   //console.log("Event fired")
   this.pser.loadBookDetails().subscribe((res: Book[])=>this.books=res);
  }

}
