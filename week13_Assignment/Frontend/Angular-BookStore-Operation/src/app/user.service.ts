import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(public http:HttpClient) { }

  loadUserDetails():Observable<User[]> {
    return this.http.get<User[]>("http://localhost:9090/users/getAllUser")
  }

  storeUserDetails(user:User):Observable<string>{
    return this.http.post("http://localhost:9090/users/storeUser",user,{responseType:'text'})
  }  

  deleteUserDetails(id:number):Observable<string>{
    return this.http.delete("http://localhost:9090/users/deleteUser/"+id,{responseType:'text'});
  }

  updateUserInfo(user:any):Observable<string>{
    return this.http.patch("http://localhost:9090/users/updateUser",user,{responseType:'text'})
  }
  
}
