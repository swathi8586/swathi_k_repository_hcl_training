package com.greatlearning.assignment.bean;

public class Book {
	
	int id;
	String Name;
	Double Price;
	public String Genre;
	int noOfCopyesSold;
	public String bookstatus;
	
		
	
	public Book() {
		super();
		
	}



	public Book(int id, String name, Double price, String genre, int noOfCopyesSold, String bookstatus) {
		super();
		this.id = id;
		Name = name;
		Price = price;
		Genre = genre;
		this.noOfCopyesSold = noOfCopyesSold;
		this.bookstatus = bookstatus;
		
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return Name;
	}



	public void setName(String name) {
		Name = name;
	}



	public Double getPrice() {
		return Price;
	}



	public void setPrice(Double price) {
		Price = price;
	}



	public String getGenre() {
		return Genre;
	}



	public void setGenre(String genre) {
		Genre = genre;
	}



	public int getNoOfCopyesSold() {
		return noOfCopyesSold;
	}



	public void setNoOfCopyesSold(int noOfCopyesSold) {
		this.noOfCopyesSold = noOfCopyesSold;
	}



	public String getBookstatus() {
		return bookstatus;
	}



	public void setBookstatus(String bookstatus) {
		this.bookstatus = bookstatus;
	}



	@Override
	public String toString() {
		return "Book [id=" + id + ", Name=" + Name + ", Price=" + Price + ", Genre=" + Genre + ", noOfCopyesSold="
				+ noOfCopyesSold + ", bookstatus=" + bookstatus + "]";
	}
	
	
}