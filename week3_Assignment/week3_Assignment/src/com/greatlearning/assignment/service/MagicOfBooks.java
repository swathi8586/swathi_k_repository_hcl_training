package com.greatlearning.assignment.service;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;



import com.greatlearning.assignment.bean.Book;

public class MagicOfBooks {
	
	Scanner scanner=new Scanner(System.in);
    
	 
    HashMap<Integer,Book> bookmap=new HashMap<>();
	 TreeMap<Double,Book> treemap=new TreeMap<>();
	 
	 ArrayList<Book> booklist=new ArrayList<>();
   
	public  void addbook(){
		
    	Book b= new Book();
    	System.out.println("Enter a book id");
    	b.setId(scanner.nextInt());
    	
		System.out.println("enter a book name");
		b.setName(scanner.next());

		System.out.println("enter a book price");
		b.setPrice(scanner.nextDouble());

		System.out.println("enter a book genre");
		b.setGenre(scanner.next());
		
		System.out.println("enter number of copies sold");
		b.setNoOfCopyesSold(scanner.nextInt());
		
		System.out.println("enter a book status");
		b.setBookstatus(scanner.next());
		
		bookmap.put(b.getId(), b);
		System.out.println("Book added successfully");
		
		treemap.put(b.getPrice(), b);
		booklist.add(b);
		
    }
    
    public void deletebook() {
    	
    	System.out.println("Enter book id you want to delete");
    	int id=scanner.nextInt();
    	bookmap.remove(id);
    	
    	 }
    
    public void updatebook() {

    	
    	Book b= new Book();
    	System.out.println("Enter a book id");
    	b.setId(scanner.nextInt());
    	
		System.out.println("enter a book name");
		b.setName(scanner.next());

		System.out.println("enter a book price");
		b.setPrice(scanner.nextDouble());

		System.out.println("enter a book genre");
		b.setGenre(scanner.next());
		
		System.out.println("enter number of copies sold");
		b.setNoOfCopyesSold(scanner.nextInt());
		
		System.out.println("enter a book status");
		b.setBookstatus(scanner.next());
		
		bookmap.replace(b.getId(), b);
		System.out.println("Book details Updated successfully");
		
	}
    
	public void displayBookInfo() {

		if (bookmap.size() > 0) 
		{
			Set<Integer> keySet = bookmap.keySet();

			for (Integer key : keySet) {

				System.out.println(key + " ----> " + bookmap.get(key));
			}
		} else {
			System.out.println("booksMap is Empty");
		}

	}

	public void count() {
		
		System.out.println("Number of books present in a store"+bookmap.size());
		
	}	
	
	public void autobiography() {
	       String bestSelling = "Autobiography";
        
        ArrayList<Book> GenreBookList = new ArrayList<Book>();
	         
	         Iterator<Book> iter=(booklist.iterator());
	         
	         while(iter.hasNext())
	         {
	             Book b1=(Book)iter.next();
	             if(b1.Genre.equals(bestSelling)) 
	             {
	            	 GenreBookList.add(b1);
	            	 System.out.println(GenreBookList);
	             }
	         }  
	}
	
	public void displayByFeature(int flag) {
		
	if(flag==1)
	{
		if (treemap.size() > 0) 
			{
			  Set<Double> keySet = treemap.keySet();
   			  for (Double key : keySet) {
				System.out.println(key + " ----> " + treemap.get(key));
   			  }
		} else {
					System.out.println("treeMap is Empty");
				}
	}
	if(flag==2) 
	{
		 Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
		  NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
		      
		  System.out.println("Book Details:");
		      
		   if (nmap.size() > 0) 
		     {
				Set<Double> keySet = nmap.keySet();
					for (Double key : keySet) {
							System.out.println(key + " ----> " + nmap.get(key));
						}
				}else{
					System.out.println("treeMap is Empty");
				}
		
	 }
	
	if(flag==3) 
	{
	    String bestSelling = "good";
        ArrayList<Book> GenreBookList = new ArrayList<Book>();
	         
	         Iterator<Book> iter=booklist.iterator();
	         
	         while(iter.hasNext())
	         {
	             Book b=(Book)iter.next();
	             if(b.bookstatus.equals(bestSelling)) 
	             {
	            	 GenreBookList.add(b);
	            	
	             }
		         
              }
	         System.out.println(GenreBookList);
	         
	   }
    }	


}  
		
	
	
    
	
	



	
	


	


		