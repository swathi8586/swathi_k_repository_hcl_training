package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;


import com.bean.User;
import com.dao.UserDao;
import com.service.UserService;

@SpringBootTest
class UserServiceTest {
	@InjectMocks
	UserService userservice;

	@Mock
	UserDao userDao;

	
	@Test
	public void addUserInfoTest() {
		User u = new User();
		userservice.addUserInfo(u);
		verify(userDao, times(1)).save(u);
	}

	
}
