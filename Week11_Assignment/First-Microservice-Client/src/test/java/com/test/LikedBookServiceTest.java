package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.bean.Book;
import com.bean.LikedBook;
import com.dao.LikedBookDao;
import com.service.LikedBookService;

@SpringBootTest
class LikedBookServiceTest {
	
	@InjectMocks
	LikedBookService likedbookservice;

	@Mock
	LikedBookDao likedbookDao;

	@Test
	public void getAlllikedbook() {
		List<LikedBook> list = new ArrayList<LikedBook>();
		LikedBook book1 = new LikedBook();
		list.add(book1);
		when(likedbookDao.findAll()).thenReturn(list);
		List<LikedBook> bookList = likedbookservice.getAlllikedbook();
		assertEquals(1, bookList.size());
		verify(likedbookDao, times(1)).findAll();
	}

    @Test
    @Disabled
	public void storeLikedBookInfoTest() {
		LikedBook book = new LikedBook();
		likedbookservice.storeLikedBookInfo(book);
		verify(likedbookDao, times(1)).save(book);
	}



}
