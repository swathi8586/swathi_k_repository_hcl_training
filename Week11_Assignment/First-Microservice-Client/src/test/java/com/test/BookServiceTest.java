package com.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bean.Book;
import com.dao.BookDao;
import com.service.BookService;

@SpringBootTest
class BookServiceTest {

	@InjectMocks
	BookService bookservice;

	@Mock
	BookDao bookDao;

	
	@Test
	public void getAllbookTest() {
		List<Book> list = new ArrayList<Book>();
		Book book1 = new Book();
		Book book2 = new Book();
		Book book3 = new Book();
		list.add(book1);
		list.add(book2);
		list.add(book3);
		when(bookDao.findAll()).thenReturn(list);
		List<Book> bookList = bookservice.getAllbook();
		assertEquals(3, bookList.size());
		verify(bookDao, times(1)).findAll();
	}


}
