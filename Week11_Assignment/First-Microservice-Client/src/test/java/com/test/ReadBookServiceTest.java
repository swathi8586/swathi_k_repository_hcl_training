package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.bean.ReadBook;
import com.dao.ReadBookDao;
import com.service.ReadBookService;
@SpringBootTest
class ReadBookServiceTest {
	

	@InjectMocks
	ReadBookService readbookservice;

	@Mock
	ReadBookDao readbookDao;

	@Test
	public void getAllreadbook() {
		List<ReadBook> list = new ArrayList<ReadBook>();
		ReadBook book1 = new ReadBook();
		list.add(book1);
		when( readbookDao.findAll()).thenReturn(list);
		List<ReadBook> bookList = readbookservice.getAllreadbook();
		assertEquals(1, bookList.size());
		verify( readbookDao, times(1)).findAll();
	}

	

	@Test
	@Disabled
	public void storeReadBookInfoTest() {
		ReadBook book = new ReadBook();
		readbookservice.storeReadBookInfo(book);
		verify(readbookDao, times(1)).save(book);
	}

	

}
