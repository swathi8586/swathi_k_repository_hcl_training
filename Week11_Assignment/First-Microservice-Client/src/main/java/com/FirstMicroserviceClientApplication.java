package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com")
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.dao")
@EntityScan(basePackages = "com.bean")

public class FirstMicroserviceClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstMicroserviceClientApplication.class, args);
		System.err.println("the first microservice client runing on port number 8282");
	}

}
