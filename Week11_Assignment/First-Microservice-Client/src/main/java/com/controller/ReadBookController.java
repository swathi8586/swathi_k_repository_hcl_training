package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.LikedBook;
import com.bean.ReadBook;
import com.service.LikedBookService;
import com.service.ReadBookService;

@RestController
@RequestMapping("/readbook")
public class ReadBookController {
	
	@Autowired
	ReadBookService readbookService;
	
	@GetMapping(value = "getAllReadBook",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<ReadBook> getAllreadbook() {
				return readbookService.getAllreadbook();
			}

	@PostMapping(value = "storeReadBook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeReadBookInfo(@RequestBody ReadBook readbook) {
		
				return readbookService.storeReadBookInfo(readbook);
	}
}
	