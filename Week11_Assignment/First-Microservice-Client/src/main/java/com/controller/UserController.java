package com.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.User;
import com.service.UserService;

@RestController
@RequestMapping("/user")
@EntityScan(basePackages = "com.bean")
public class UserController {
	
	@Autowired
	UserService userservice;

	@PostMapping(value ="AddUser",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String insertUser(@RequestBody User user)
	{
		return userservice.addUserInfo(user);
	}

	@PostMapping(value = "login",consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.TEXT_PLAIN_VALUE)
	public String userDetails(@RequestBody User user)
	{
		User find =  userservice.login(user.getEmail(),user.getPassword());
		if(Objects.nonNull(find))
		{
			return "login successfull";
		}
		else {
			return "login failed";
		}
	}

	@PostMapping(value = "logout",consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.TEXT_PLAIN_VALUE)
	public String userLogout(HttpSession hs)
	{
		hs.invalidate();
		return "logout successfully";
	}
	}

