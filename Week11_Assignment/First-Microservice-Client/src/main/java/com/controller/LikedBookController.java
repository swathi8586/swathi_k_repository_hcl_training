package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.LikedBook;
import com.service.LikedBookService;

@RestController
@RequestMapping("/likedbook")
public class LikedBookController {
	
	@Autowired
	LikedBookService likedbookService;
	
	@GetMapping(value = "getAllLikedBook",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<LikedBook> getAlllikedbook() {
				return likedbookService.getAlllikedbook();
			}

	@PostMapping(value = "storelikedBook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeLikedBookInfo(@RequestBody LikedBook likedbook) {
		
				return likedbookService.storeLikedBookInfo(likedbook);
	}
}
	
