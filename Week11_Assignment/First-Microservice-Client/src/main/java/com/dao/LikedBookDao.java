package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bean.LikedBook;

@Repository
public interface LikedBookDao extends JpaRepository<LikedBook, Integer>{
	

	
}

