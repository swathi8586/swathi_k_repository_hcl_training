package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bean.ReadBook;

@Repository
public interface ReadBookDao extends JpaRepository<ReadBook, Integer>{

	
}
