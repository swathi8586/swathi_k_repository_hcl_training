package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.LikedBook;
import com.dao.LikedBookDao;

@Service
public class LikedBookService {

	@Autowired
	LikedBookDao likedbookDao;
	public List<LikedBook> getAlllikedbook() {
		return likedbookDao.findAll();
	}
	
	public String storeLikedBookInfo(LikedBook likedbook) {
		
		if(likedbookDao.existsById(likedbook.getId())) {
					return "Book id must be unique";
		}else {
					likedbookDao.save(likedbook);
					return "Book stored successfully";
		}
		
	}	

}
