package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bean.ReadBook;
import com.dao.ReadBookDao;

@Service
public class ReadBookService {

	@Autowired
	ReadBookDao readbookDao;
	public List<ReadBook> getAllreadbook() {
		return readbookDao.findAll();
	}
	
	public String storeReadBookInfo(ReadBook readbook) {
		
		if(readbookDao.existsById(readbook.getId())) {
					return "Book id must be unique";
		}else {
					readbookDao.save(readbook);
					return "Book stored successfully";
		}
		
	}	

}

