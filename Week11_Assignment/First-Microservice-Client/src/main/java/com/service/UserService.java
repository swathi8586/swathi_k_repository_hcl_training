package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.User;
import com.dao.UserDao;

@Service

public class UserService {
	@Autowired
	UserDao userdao;
	
	
public User login(String email,String password) {
		User user = userdao.findByEmailAndPassword(email,password);
		return user;
		
	}
	public String addUserInfo(User user) {
		if(userdao.existsById(user.getEmail()))
		{
			return "This mail id already present";
		}
		else {
			userdao.save(user);
			return"user saved successfully";
		}
	}
	

}
