package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.bean.Book;
import com.bean.User;
import com.dao.BookDao;
import com.dao.UserDao;
import com.service.BookService;
import com.service.UserService;
@SpringBootTest
class UserServiceTest {
	
	@InjectMocks
	UserService userservice;

	@Mock
	UserDao userDao;

	@Test
	//@Disabled
	public void storeUserInfoTest() {
		User user = new User();
		userservice.storeUserInfo(user);
		verify(userDao, times(1)).save(user);
	}

	@Test
	//@Disabled
	public void getAlluserTest() {
		List<User> list = new ArrayList<User>();
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		list.add(user1);
		list.add(user2);
		list.add(user3);
		when(userDao.findAll()).thenReturn(list);
		List<User> userList = userservice.getAlluser();
		assertEquals(3, userList.size());
		verify(userDao, times(1)).findAll();
	}
	
	@Test
	
	public void updateUserInfoTest() {
		User user = new User(1, "arya", "a@gmail.com","321");
		when(userDao.findById(user.getId())).thenReturn(Optional.of(user));
		userservice.updateUserInfo(user);
		verify(userDao, times(1)).saveAndFlush(user);
	}

	@Test
	//@Disabled
	public void deleteUserInfoTest() {
		User user = new User(2, "arya", "a@gmail.com","123");
		when(userDao.findById(user.getId())).thenReturn(Optional.of(user));
		assertEquals("Deleted Successfully", userservice.deleteUserInfo(2));
		verify(userDao, times(1)).delete(user);
	}

}

	

