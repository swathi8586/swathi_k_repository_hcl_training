package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.bean.Admin;
import com.bean.User;
import com.dao.AdminDao;
import com.dao.UserDao;
import com.service.AdminService;


@SpringBootTest
class AdminServiceTest {

	@InjectMocks
	AdminService adminservice;

	@Mock
	AdminDao adminDao;

	
	@Test
	public void addAdminInfoTest() {
		Admin u = new Admin();
		adminservice.addAdminInfo(u);
		verify(adminDao, times(1)).save(u);
	}

	


}
