package com.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.bean.Book;
import com.bean.User;
import com.dao.BookDao;
import com.service.BookService;

class BookServiceTest {
	
	@Mock
	BookDao bookDao;
	
	@InjectMocks
	BookService bookservice;

	@Test
	public void storeBookInfoTest() {
		Book book = new Book();
		bookservice.storeBookInfo(book);
		verify(bookDao, times(1)).save(book);
	}

	@Test
	public void getAllbookTest() {
		List<Book> list = new ArrayList<Book>();
		Book book1 = new Book();
		Book book2 = new Book();
		Book book3 = new Book();
		list.add(book1);
		list.add(book2);
		list.add(book3);
		when(bookDao.findAll()).thenReturn(list);
		List<Book> bookList = bookservice.getAllbook();
		assertEquals(3, bookList.size());
		verify(bookDao, times(1)).findAll();
	}
	
	@Test
	public void updateBookInfoTest() {
		Book book = new Book(1,"Invisible Man","Novel");
		when(bookDao.findById(book.getId())).thenReturn(Optional.of(book));
		bookservice.updateBookInfo(book);
		verify(bookDao, times(1)).saveAndFlush(book);
	}

	@Test
	public void deleteBookInfoTest() {
		Book book = new Book(1,"Invisible Man","History");
		when(bookDao.findById(book.getId())).thenReturn(Optional.of(book));
		assertEquals("Deleted Successfully", bookservice.deleteBookInfo(1));
		verify(bookDao, times(1)).delete(book);
	}
}
