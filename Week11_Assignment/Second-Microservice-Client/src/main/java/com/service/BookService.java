package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Book;
import com.dao.BookDao;

@Service
public class BookService {
	
	@Autowired
	BookDao bookDao;
	public List<Book> getAllbook() {
		return bookDao.findAll();
	}
	
	public String storeBookInfo(Book book) {
		
		if(bookDao.existsById(book.getId())) {
					return "Book id must be unique";
		}else {
					bookDao.save(book);
					return "Book stored successfully";
		}
		
	}	

	public String deleteBookInfo(int id) {
		if(!bookDao.existsById(id)) {
			return "Book details not present";
			}else {
				bookDao.deleteById(id);
				return "Book deleted successfully";
			}
	
	}

	public String updateBookInfo(Book book) {
		if(!bookDao.existsById(book.getId())) {
			return "Book details not present";
			}else {
			Book b	= bookDao.getById(book.getId());	
			b.setCategory(book.getCategory());						
			bookDao.saveAndFlush(b);				
			return "Book updated successfully";
			}	
	}
	
}


	



