package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Book;
import com.bean.User;
import com.dao.UserDao;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public List<User> getAlluser() {
		return userDao.findAll();
	}
	
	public String storeUserInfo(User user) {
		
		if(userDao.existsById(user.getId())) {
					return "user id must be unique";
		}else {
					userDao.save(user);
					return "user stored successfully";
		}
		
	}

	public String deleteUserInfo(int id) {
		
		
		if(!userDao.existsById(id)) {
			return "User details not present";
			}else {
				userDao.deleteById(id);
				return "User deleted successfully";
			}	
	

	}

	public String updateUserInfo(User user) {
		if(!userDao.existsById(user.getId())) {
			return "user details not present";
			}else {
			User u	= userDao.getById(user.getId());	// if product not present it will give exception 
			u.setName(user.getName());						// existing product price change 
			userDao.saveAndFlush(u);				// save and flush method to update the existing product
			return "user updated successfully";
			}	

}

}
