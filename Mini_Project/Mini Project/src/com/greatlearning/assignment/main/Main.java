package com.greatlearning.assignment.main;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.greatlearning.assignment.bean.Item;
import com.greatlearning.assignment.service.Bill;

public class Main {

	
	public static void main(String[] args) {
		
		
		
		Scanner sc = new Scanner(System.in);
		Date time=new Date();
		List<Bill> bills = new ArrayList<Bill>();
		boolean finalOrder;

		List<Item> items = new ArrayList<Item>();

		Item i1 = new Item(1, "Rajma Chawal", 2, 150.0);
		Item i2 = new Item(2, "Momos", 2, 190.0);
		Item i3 = new Item(3, "Red Thai Curry", 2, 180.0);
		Item i4 = new Item(4, "Chaap", 2, 190.0);
		Item i5 = new Item(5, "Chilli Potato", 1, 250.0);

		items.add(i1);
		items.add(i2);
		items.add(i3);
		items.add(i4);
		items.add(i5);

		System.out.println(
				"Welcome to Surabi Restaurant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		while (true) {
			System.out.println("Please Enter the Credentials");

			System.out.println("Email = ");
			String email = sc.next();

			System.out.println("Password = ");
			String password = sc.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please Enter A if you are Admin and U if you are User ,L to logout");
			String b= sc.next();

			Bill bill = new Bill();
			List<Item> selectedItems = new ArrayList<Item>();
			
			double totalCost = 0;
			
			Date date=new Date();
			
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			String currentTime = time1.format(f);

			if (b.equals("U") || b.equals("u")) {

				System.out.println("Welcome Mr. " + name);
				do {
					System.out.println("Today's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int code = sc.nextInt();
					
					if (code == 1) {
						selectedItems.add(i1);
						totalCost += i1.getItemPrice();
					}

					else if (code == 2) {
						selectedItems.add(i2);
						totalCost += i2.getItemPrice();
					} else if (code == 3) {
						selectedItems.add(i3);
						totalCost += i3.getItemPrice();
					} else if (code == 4) {
						selectedItems.add(i4);
						totalCost += i4.getItemPrice();
					} else {
						selectedItems.add(i5);
						totalCost += i5.getItemPrice();
					}
					
					System.out.println("Press 0 to show bill\nPress 1 to order more");
					int opt = sc.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);
				

				System.out.println("Thanks Mr " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				bills.add(bill);

			} else if (b.equals("A") || b.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = sc.nextInt();
				switch (option) {
				case 1:
					if ( !bills.isEmpty()) {
						for (Bill b1 : bills) {
							if(b1.getTime().getDate()==time.getDate()) {
							System.out.println("\nUsername :- " + b1.getName());
							System.out.println("Items :- " + b1.getItems());
							System.out.println("Total :- " + b1.getCost());
							System.out.println("Date " + b1.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills today.!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (Bill b1 : bills) {
							if (b1.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUsername :- " + b1.getName());
							System.out.println("Items :- " + b1.getItems());
							System.out.println("Total :- " + b1.getCost());
							System.out.println("Date " + b1.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Bill b1 : bills) {
							System.out.println("\nUsername :- " + b1.getName());
							System.out.println("Items :- " + b1.getItems());
							System.out.println("Total :- " + b1.getCost());
							System.out.println("Date " + b1.getTime() + "\n");
						}
					} else
						System.out.println("No Bills.!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (b.equals("L") || b.equals("l")) {
				System.exit(1);
				
			}else  {
				System.out.println("Invalid Entry");
			}

		}

	}

	
}
