create database mvc;
use mvc;

create table book(id int primary key,name varchar(10),author_name varchar(10),category varchar(10));
create table users(name varchar(10),email varchar(10),password varchar(10));
create table likedbooksection(id int primary key,name varchar(10),author_name varchar(10),category varchar(10));
create table readlatersection(id int primary key,name varchar(10),author_name varchar(10),category varchar(10));

insert into book values( 1,"Invisible Man","J.K Rowling","Novel");
insert into book values( 2 ,"Scotland street","George","Comic");
insert into book values( 3 ,"Culinary Arts","Mark","cooking");
insert into book values( 4 ,"Brain Teasers","Charles","History");
insert into book values( 5 ,"Crafts","Kurt","Activity");

insert into users values("swathi","swathik.8586@gmail.com","222");

insert into users values("a","a@gmail.com","898");

insert into users values("b","b@gmail.com","123");


insert into likedbooksection( 1 ,"Invisible Man","J.K Rowling","Novel");

insert into likedbooksection( 4 ,"Brain Teasers","Charles","History");

insert into readlatersection( 2,"Scotland street","George","Comic");

insert into readlatersection( 5,"Crafts","Kurt","Activity");

select * from book;

select * from users;

select * from likedbooksection;

select * from readlatersection;






