package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Book;
import com.service.BookDisplayService;

@Controller
public class BookDisplayController {

	@Autowired
	BookDisplayService bookdisplayService;

	@GetMapping(value = "display")
	public ModelAndView getAllBook(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		List<Book> listOfBook = bookdisplayService.getAllBook();
		req.setAttribute("books", listOfBook);
		mav.setViewName("display.jsp");
		return mav;
	}

}

