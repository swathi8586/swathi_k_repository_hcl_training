package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Book;
import com.dao.BookDisplayDao;

@Service
public class BookDisplayService {

	@Autowired
	BookDisplayDao bookdisplayDao;
	
	public List<Book> getAllBook() {
		return bookdisplayDao.getAllBook();
	}
}

