package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Book;

@Repository
public class BookDisplayDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Book> getAllBook() {
		return jdbcTemplate.query("select * from book", new bookRowMapper());
	}
	
	

}

class bookRowMapper implements RowMapper<Book>{
	@Override
	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Book b = new Book();
		b.setId(rs.getInt(1));
		b.setName(rs.getString(2));
		b.setAuthor_name(rs.getString(3));
		b.setCategory(rs.getString(4));
		
		return b;
	}
	
	
}

