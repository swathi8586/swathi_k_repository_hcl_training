package com.bean;

public class Book {

private int id;
private String name;
private String author_name;
private String Category;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAuthor_name() {
	return author_name;
}
public void setAuthor_name(String author_name) {
	this.author_name = author_name;
}
public String getCategory() {
	return Category;
}
public void setCategory(String category) {
	Category = category;
}
@Override
public String toString() {
	return "Book [id=" + id + ", name=" + name + ", author_name=" + author_name + ", Category=" + Category + "]";
}



}
