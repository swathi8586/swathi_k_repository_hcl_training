<%@page import ="com.controller.BookController"%>
<%@page import ="java.sql.*"%>
<%
String id = request.getParameter("id");
String name = request.getParameter("name");
String author_name = request.getParameter("author_name");
String category = request.getParameter("category");

try{
	
	Connection con = BookController.getCon();
	  PreparedStatement st = con.prepareStatement("insert into book values(?,?,?,?)");
	  st.setString(1,id);
	  st.setString(2,name);
	  st.setString(3,author_name);
	  st.setString(4,category);
	  st.executeUpdate();
	  response.sendRedirect("addbook.jsp?msg=done");
	  
}
catch(Exception e){
	

 response.sendRedirect("addbook.jsp?msg=wrong");
}
%>