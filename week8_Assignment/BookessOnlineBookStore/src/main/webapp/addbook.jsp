<%@page import ="com.controller.BookController"%>
<%@page import ="java.sql.*"%>
<%@include file = "header.jsp" %>

<html>
<head>

<title>Add book</title>
</head>
<body>
<% 
String msg = request.getParameter("msg");
if("done".equals(msg))
{
%>
<h3 class="alert">books Added Successfully!</h3>
<%} %>
<% 
if("wrong".equals(msg))
{
%>
<h3 class="alert">Some thing went wrong! Try Again!</h3>
<%}%>
<% 
int id =1;
try{
	 Connection con = BookController.getCon();
	  Statement st = con.createStatement();
	  ResultSet rs = st.executeQuery("select max(id) from book");
	  while(rs.next()){
		  id = rs.getInt(1);
		  id=id+1;
	  }
}catch(Exception e){
	
}
%>

<form action ="addbookAction.jsp" method ="post">
<h3 style="color: yellow;">Book ID: <%out.println(id); %> </h3>
<input type ="hidden" name ="id" value ="<%out.println(id); %>">

<div class="left-div">
 <h3>Enter Name</h3>
 <input class = "input-style" type ="text" name="name" placeholder="Enter Name" required>
 
<hr>
</div>

<div class="right-div">
<h3>Enter Category</h3>
 <input class = "input-style" type ="text" name="category" placeholder="Enter category" required>
<hr>
</div>

<button class = "button">Save <i class='far fa-arrow-alt-circle-right'></i> </button>
</form>
</body>
<br><br><br>
</body>
</html>