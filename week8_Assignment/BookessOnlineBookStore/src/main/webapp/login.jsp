<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="CSS/signup-style.css">
<title>Login</title>
</head>
<body>
<div id='container'>
  <div class='signup'>
  <form action =" loginAction.jsp"method = "post">
  <h2>Bookess Online Book Store</h2>
  <br><input type = "email" name = "email" placeholder = "Enter Email" required></br>
  <br><input type = "password" name = "password" placeholder = "Enter password" required></br>
  <br><input type ="submit" value = "login"></br>  
  </form>
     
      <h2><a href="signup.jsp">SignUp</a></h2>
       <h2><a href="forgotPassword.jsp">Forgot Password?</a></h2>
  </div>
  <div class='whysignLogin'>
  <%
  String msg = request.getParameter("msg");
  if("notexist".equals(msg))
  {
 %>
  <h1>Incorrect Username or Password</h1>
  <% } %>
  <%if("invalid".equals(msg))
  {%>

<h1>Some thing Went Wrong! Try Again !</h1>
<%} %>
    
   
  </div>
</div>

</body>
</html>