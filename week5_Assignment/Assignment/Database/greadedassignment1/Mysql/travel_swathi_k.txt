create database travel_Swathi_k;
use travel_Swathi_k;

create table PASSENGER (Passenger_name varchar(20),Category varchar(20),Gender varchar(20),Boarding_City varchar(20),Destination_City varchar(20),Distance int,Bus_Type varchar(20));
create table PRICE(Bus_Type varchar(20),Distance int,Price int);

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

select gender,count(gender) from passenger where distance>=600 group by gender;
  
select min(price),Bus_Type from price where Bus_Type ='Sleeper';
 
select Passenger_name from passenger where Passenger_name LIKE 's%';

select p.Price,pa.Passenger_name,pa.Boarding_city,pa.Destination_City,pa.Bus_Type from passenger pa,price p where pa.distance = p.distance and pa.Bus_Type = p.Bus_Type GROUP BY pa.passenger_name;

Select pa.Passenger_name, p.price from passenger pa inner join price p on pa.bus_type = p.bus_type and pa.distance = p.distance where pa.bus_type = 'Sitting' and pa.distance = 1000;

Select p.passenger_name, pr.bus_type, pr.price,p.Boarding_City,p.Destination_City from passenger p, price pr where p.passenger_name = 'Pallavi' and p.distance = pr.distance and p.Boarding_City = 'Bengaluru' and p.Destination_City = 'Panaji';

select Distinct(Distance) from passenger ORDER BY Distance desc;

Select passenger_name, (distance /(Select sum(distance) from passenger) * 100) as distance_travelled  from passenger;

create view passenger_view as select Passenger_name,Category from passenger where Category = 'AC';
select * from passenger_view;

delimiter @
    create procedure totalpassengers() 
    begin 
    declare 
    select Passenger_name,Bus_Type from passenger where Bus_Type = 'Sleeper';
    end;
    @

    call totalpassengers();@

select * from passenger LIMIT 5;