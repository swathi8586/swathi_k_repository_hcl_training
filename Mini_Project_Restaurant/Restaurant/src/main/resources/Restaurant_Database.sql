create database restaurant;
use restaurant;

CREATE TABLE `admin` (
name varchar(100),email varchar(100),password varchar(100)); 


insert into admin values("admin","a@gmail.com","123");

select * from admin;


CREATE TABLE `user` (
id int primary key,name varchar(100),email varchar(100),passsword varchar(100)
);

insert into user values(1,"rajiv","rajiv@gmail.com","123");

select * from user;



CREATE TABLE `menu` (
id int primary key,name varchar(100),price int,url Varchar(1000)
);

insert into menu values(1,"French Fries",160,"https://live.staticflickr.com/8400/8643961030_228c994522_n.jpg");

select * from menu;

CREATE TABLE `cart` (
  `id` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` double NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `mrp_price` double NOT NULL
);


INSERT INTO `cart` (`id`, `price`, `quantity`, `total_price`, `customer_id`, `product_id`, `mrp_price`) VALUES
(5, 190, 1, 190, 1, 11, 200);

select * from cart;


CREATE TABLE `contact` (
  `id` bigint(20) NOT NULL,
  `contact_date` datetime NOT NULL,
  `email` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `name` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL
);



INSERT INTO `contact` (`id`, `contact_date`, `email`, `message`, `name`, `subject`) VALUES
(2, '2020-07-25 19:45:20', 'dharmeshmourya043@gmail.com', 'Hi, This is testing email contacts.', 'Dharmesh Mourya', 'Testing');

select * from contact;

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `added_date` datetime NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `valid` bit(1) NOT NULL,
  `pin_code` varchar(255) NOT NULL
);



INSERT INTO `customer` (`id`, `address`, `added_date`, `email`, `gender`, `name`, `password`, `phone`, `valid`, `pin_code`) VALUES
(1, 'Ram Nagar,Vrindavan Society, Mumbai', '2020-05-10 11:11:01', 'dharmeshmourya043@gmail.com', 'Male', 'Dharmesh', '$2a$10$QQeMRbMjnXys3cDIvvozH.dpxxCLD9d0b5Pz092djiwkSLW1YJ/Vy', '1223334560', b'1', '400330');

select * from customer;


CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `amount` double NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_address_type` varchar(15) NOT NULL,
  `customer_email` varchar(50) NOT NULL,
  `customer_name` varchar(30) NOT NULL,
  `customer_phone` varchar(10) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_num` int(11) NOT NULL,
  `pin_code` varchar(10) NOT NULL
);

INSERT INTO `orders` (`id`, `active`, `amount`, `customer_address`, `customer_address_type`, `customer_email`, `customer_name`, `customer_phone`, `order_date`, `order_num`, `pin_code`) VALUES
(237, b'1', 284, 'Ram Nagar,Vrindavan Society, Mumbai', 'Home', 'dharmeshmourya043@gmail.com', 'Dharmesh', '1223334560', '2020-07-25 19:59:26', 1000, '400330'),
(238, b'1', 95, 'Ram Nagar,Vrindavan Society, Mumbai', 'Home', 'dharmeshmourya043@gmail.com', 'Dharmesh', '1223334560', '2020-07-25 19:59:26', 1001, '400330'),
(239, b'1', 65, 'Ram Nagar,Vrindavan Society, Mumbai', 'Home', 'dharmeshmourya043@gmail.com', 'Dharmesh', '1223334560', '2020-07-25 19:59:26', 1002, '400330');

select * from orders;


CREATE TABLE `order_detail` (
  `id` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `order_status` varchar(25) NOT NULL,
  `mrp_price` double NOT NULL,
  `payment_mode` varchar(25) NOT NULL
);



INSERT INTO `order_detail` (`id`, `amount`, `price`, `quantity`, `order_id`, `product_id`, `payment_id`, `order_status`, `mrp_price`, `payment_mode`) VALUES
(232, 284, 142, 2, 237, 10, 1000, 'Delivered', 150, 'COD'),
(233, 95, 95, 1, 238, 9, 1000, 'Delivered', 100, 'COD'),
(234, 65, 65, 1, 239, 5, 1000, 'Delivered', 72, 'COD');

select * from order_detail; 

CREATE TABLE `product` (
`id` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `code` varchar(5) NOT NULL,
  `create_date` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `price` double NOT NULL,
  `mrp_price` double NOT NULL
);



insert into product values(1, b'1', 'P01', '2020-05-15 02:58:36', 'Paneer Tikka', 'https://tse4.mm.bing.net/th?id=OIP.bVVoyJXjYzvRRAtkP0ezgwHaFj&pid=Api&P=0&w=219&h=164', 'Paneer Tikka', 99, 105),
(2, b'1', 'P02', '2020-05-15 02:59:56', 'French Fries', 'https://live.staticflickr.com/8400/8643961030_228c994522_n.jpg', 'French Fries', 160, 168),
(3, b'1', 'P03', '2020-05-15 03:12:16', 'Pizza', 'https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=', 'Pizza', 160, 165),
(4, b'1', 'P04', '2020-05-15 03:03:41', 'Pao Bhaji', 'https://static.toiimg.com/thumb/52416693.cms?imgsize=789478&width=800&height=800', 'Pao Bhaji', 160, 165);

select * from product;


ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_c0r9atamxvbhjjvy5j8da1kam` (`email`);


ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CART_CUST_FK` (`customer_id`),
  ADD KEY `CART_PROD_FK` (`product_id`);


ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_dwk6cx0afu8bs9o4t536v1j5v` (`email`),
  ADD UNIQUE KEY `UK_o3uty20c6csmx5y4uk2tc5r4m` (`phone`);


ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_o6e714ot0hclyvhlcte6vc4mr` (`order_num`),
  ADD UNIQUE KEY `UKo6e714ot0hclyvhlcte6vc4mr` (`order_num`);


ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ORDER_DETAIL_ORD_FK` (`order_id`),
  ADD KEY `ORDER_DETAIL_PROD_FK` (`product_id`);


ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_h3w5r1mx6d0e5c6um32dgyjej` (`code`);


ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `cart`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `contact`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

ALTER TABLE `order_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;


