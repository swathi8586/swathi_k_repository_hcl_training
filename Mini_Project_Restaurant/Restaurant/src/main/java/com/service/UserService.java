package com.service;

import java.util.List;

import com.entity.User;

public interface UserService {

	List<User> getAlluser();

	String storeUserInfo(User user);

	String deleteUserInfo(int id);

	String updateUserInfo(User user);

}
