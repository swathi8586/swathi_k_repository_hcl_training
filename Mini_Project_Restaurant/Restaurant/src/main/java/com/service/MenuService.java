package com.service;

import java.util.List;

import com.entity.Menu;

public interface MenuService {

	List<Menu> getAllmenu();

	String storeMenuInfo(Menu menu);

	String deleteMenuInfo(int id);

	String updateMenuInfo(Menu menu);

	

}
