package com.service;

import java.util.Optional;

import com.entity.Admin;


public interface AdminService {

	Admin login(String email, String password);
	String addAdminInfo(Admin admin);
	
}
