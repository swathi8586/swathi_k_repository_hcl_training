package com.service;

import javax.mail.MessagingException;
import com.entity.Contact;

public interface ContactService {
	
	void saveContacts(Contact contact) throws MessagingException;

}
