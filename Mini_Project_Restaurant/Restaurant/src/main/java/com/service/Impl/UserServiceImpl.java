package com.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.User;
import com.repository.UserRepository;

import com.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userDao;
	
	@Override
	public List<User> getAlluser() {
		return userDao.findAll();
	}
	
	@Override
	public String storeUserInfo(User user) {
		
		if(userDao.existsById(user.getId())) {
					return "user id must be unique";
		}else {
					userDao.save(user);
					return "user stored successfully";
		}
		
	}

	@Override
	public String deleteUserInfo(int id) {
		
		
		if(!userDao.existsById(id)) {
			return "User details not present";
			}else {
				userDao.deleteById(id);
				return "User deleted successfully";
			}	
	

	}

	@Override
	public String updateUserInfo(User user) {
		if(!userDao.existsById(user.getId())) {
			return "user details not present";
			}else {
			User u	= userDao.getById(user.getId());	// if product not present it will give exception 
			u.setName(user.getName());						// existing product price change 
			userDao.saveAndFlush(u);				// save and flush method to update the existing product
			return "user updated successfully";
			}	

}

}
