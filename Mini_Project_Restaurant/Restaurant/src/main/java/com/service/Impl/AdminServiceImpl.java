package com.service.Impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.Admin;
import com.repository.AdminRepository;
import com.service.AdminService;


@Service
@Transactional
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminRepository adminRepository;

	@Override
	public Admin login(String email,String password) {
		Admin admin = adminRepository.findByEmailAndPassword(email,password);
		return admin;
		
	}

	
	public String addAdminInfo(Admin admin) {
		if(adminRepository.existsById(admin.getEmail()))
		{
			return "This mail id already present";
		}
		else {
			adminRepository.save(admin);
			return"admin saved successfully";
		}
	}


	
}
