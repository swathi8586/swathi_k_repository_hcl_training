package com.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.Menu;
import com.repository.MenuRepository;
import com.service.MenuService;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	MenuRepository menuDao;
	
	@Override
	public List<Menu> getAllmenu() {
		return menuDao.findAll();
	}
	
	@Override
	public String storeMenuInfo(Menu menu) {
		
		if(menuDao.existsById(menu.getId())) {
					return "menu id must be unique";
		}else {
			menuDao.save(menu);
					return "menu stored successfully";
		}
		
	}

	@Override
	public String deleteMenuInfo(int id) {
		
		
		if(!menuDao.existsById(id)) {
			return "Menu details not present";
			}else {
				menuDao.deleteById(id);
				return "Menu deleted successfully";
			}	
	

	}

	@Override
	public String updateMenuInfo(Menu menu) {
		if(!menuDao.existsById(menu.getId())) {
			return "menu details not present";
			}else {
			Menu u	= menuDao.getById(menu.getId());	// if product not present it will give exception 
			u.setName(menu.getName());						// existing product price change 
			menuDao.saveAndFlush(u);				// save and flush method to update the existing product
			return "menu updated successfully";
			}	

}


}
