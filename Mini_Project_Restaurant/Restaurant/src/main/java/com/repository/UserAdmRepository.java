package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Customer;

public interface UserAdmRepository extends JpaRepository<Customer,Long>
{

}
