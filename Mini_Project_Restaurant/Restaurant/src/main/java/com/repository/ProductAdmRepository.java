package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Product;

public interface ProductAdmRepository extends JpaRepository<Product, Long> {
}
