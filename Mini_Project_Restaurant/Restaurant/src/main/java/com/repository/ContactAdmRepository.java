package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Contact;

public interface ContactAdmRepository extends JpaRepository<Contact, Long> 
{
	
}
