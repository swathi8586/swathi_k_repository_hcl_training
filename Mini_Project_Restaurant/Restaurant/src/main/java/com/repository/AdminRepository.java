package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Admin;
@Repository
public interface AdminRepository extends JpaRepository<Admin, String> {
	Admin findByEmailAndPassword(@Param("email")String email,@Param("password")String password);	
	}

	

