package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.User;
import com.service.UserService;

@RestController
@RequestMapping("/users")
@EntityScan(basePackages = "com.entity")

public class UserController {
	
	@Autowired
	UserService userService;
	

	@GetMapping(value = "getAllUser",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<User> getAllUserInfo() {
				return userService.getAlluser();
			}

	@PostMapping(value = "storeUser",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) {
		
				return userService.storeUserInfo(user);
	}
	
	@DeleteMapping(value = "deleteUser/{id}")
	public String deleteUserInfo(@PathVariable("id") int id) {
					return userService.deleteUserInfo(id);
	}
	
	@PatchMapping(value = "updateUser")
	public String updateUserInfo(@RequestBody User user) {
					return userService.updateUserInfo(user);
	}
	



}

