package com.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


import com.entity.Admin;
import com.entity.OrderDetail;
import com.service.AdminService;
import com.service.OrderDetailAdmService;



@RestController
@RequestMapping("/admin")
@EntityScan(basePackages = "com.entity")
public class AdminController {

	private static final Logger log = LoggerFactory.getLogger(AdminController.class);

	
	@Autowired
	private AdminService adminservice;
	
	@PostMapping(value ="AddAdmin",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String insertAdmin(@RequestBody Admin admin)
	{
		return adminservice.addAdminInfo(admin);
	}

	@PostMapping(value = "login",consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.TEXT_PLAIN_VALUE)
	public String adminDetails(@RequestBody Admin admin)
	{
		Admin find =  adminservice.login(admin.getEmail(),admin.getPassword());
		if(Objects.nonNull(find))
		{
			return "login successg=fully";
		}
		else {
			return "login failed";
		}
	}

	@PostMapping(value = "logout",consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.TEXT_PLAIN_VALUE)
	public String adminLogout(HttpSession hs)
	{
		hs.invalidate();
		return "logout successfully";
	}
	}

	
	
	

