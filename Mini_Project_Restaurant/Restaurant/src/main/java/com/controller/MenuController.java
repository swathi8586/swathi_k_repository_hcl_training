package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Menu;
import com.service.MenuService;


@RestController
@RequestMapping("/menu")
@EntityScan(basePackages = "com.entity")
public class MenuController {
	
	@Autowired
	MenuService menuService;
	

	@GetMapping(value = "getAllMenu",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<Menu> getAllmenu() {
				return menuService.getAllmenu();
			}

	@PostMapping(value = "storeMenu",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeMenuInfo(@RequestBody Menu menu) {
		
				return menuService.storeMenuInfo(menu);
	}
	
	@DeleteMapping(value = "deleteMenu/{id}")
	public String deleteMenuInfo(@PathVariable("id") int id) {
		return menuService.deleteMenuInfo(id);
	}
	
	@PatchMapping(value = "updateMenu")
	public String updateMenuInfo(@RequestBody Menu menu) {
					return menuService.updateMenuInfo(menu);
	}
	


}
