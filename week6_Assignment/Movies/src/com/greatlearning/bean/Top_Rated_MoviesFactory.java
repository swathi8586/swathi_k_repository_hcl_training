package com.greatlearning.bean;

public class Top_Rated_MoviesFactory {
	public static Top_Rated_Movies getInstance(String type) {
		if(type.equalsIgnoreCase("top_rated_movies")) {
			return new Top_Rated_Movies(); 
		}else {
			return null;
		}
	}
}
