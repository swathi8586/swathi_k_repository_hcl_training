package com.greatlearning.bean;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Movies_Coming dd1 = Movies_ComingFactory.getInstance("movies_coming");
		dd1.Types_of_movies();
		System.out.println(" ");
		Movies_In_Theaters dd2 = Movies_In_TheatersFactory.getInstance("movies_in_theaters");
		dd2.Types_of_movies();
		System.out.println(" ");
		Top_Rated_India dd3 = Top_Rated_IndiaFactory.getInstance("top_rated_india");
		dd3.Types_of_movies();
		System.out.println(" ");
		Top_Rated_Movies dd4 = Top_Rated_MoviesFactory.getInstance("top_rated_movies");
		dd4.Types_of_movies();

	}

}
