package com.greatlearning.bean;

public class Top_Rated_IndiaFactory {
	public static Top_Rated_India getInstance(String type) {
		if(type.equalsIgnoreCase("top_rated_india")) {
			return new Top_Rated_India(); 
		}else {
			return null;
		}
	}
}
