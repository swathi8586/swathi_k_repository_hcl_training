package com.greatlearning.bean;

public class Movies_In_TheatersFactory {
	public static Movies_In_Theaters getInstance(String type) {
		if(type.equalsIgnoreCase("movies_in_theaters")) {
			return new Movies_In_Theaters();
		}else {
			return null;
		}
	}

}
