package com.greatlearning.bean;

public class Movies {
	private int id;
	private String title;
	private int year;
	private String genres;
	private int ratings;
	public Movies() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Movies(int id, String title, int year, String genres, int ratings) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genres = genres;
		this.ratings = ratings;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public int getRatings() {
		return ratings;
	}
	public void setRatings(int ratings) {
		this.ratings = ratings;
	}
	@Override
	public String toString() {
		return "Movies [id=" + id + ", title=" + title + ", year=" + year + ", genres=" + genres + ", ratings="
				+ ratings + "]";
	}
	
	

}
