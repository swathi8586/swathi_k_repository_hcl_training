package com.greatlearning.singletontest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.greatlearning.singletonservice.MoviesComingSingletonService;
import com.greatlearning.singletonservice.MoviesInTheatersSingletonService;

public class MoviesInTheatersSingletonTest {
	@Test
			public void testCheck() {
				//fail("Not yet implemented");
				 MoviesInTheatersSingletonService mc = new  MoviesInTheatersSingletonService(); 
				String successResult = mc.check("party","Comedy");
				String failureResult = mc.check("ugly", "comedy");
				assertEquals("success", successResult);
				assertEquals("failure", failureResult);
			}

			@Test
			public void testCheck1() {
				//fail("Not yet implemented");
				 MoviesInTheatersSingletonService mc = new  MoviesInTheatersSingletonService(); 
				String successResult1 = mc.check1("Samson","8");
				String failureResult1 = mc.check1("ugly", "123");
				assertEquals("successed", successResult1);
				assertEquals("failed", failureResult1);
			}	
			
			@Test
			public void testCheck2() {
				//fail("Not yet implemented");
				MoviesInTheatersSingletonService mc = new MoviesInTheatersSingletonService(); 
				String successResult1 = mc.check2("Samson","2019");
				String failureResult1 = mc.check2("Dogs", "2008");
				assertEquals("successed", successResult1);
				assertEquals("failed", failureResult1);
			}
			
			@Test
			public void testCheck3() {
				//fail("Not yet implemented");
				MoviesInTheatersSingletonService mc = new MoviesInTheatersSingletonService(); 
				String successResult1 = mc.check3("Comedy","10");
				String failureResult1 = mc.check3("Comedy", "11");
				assertEquals("successed", successResult1);
				assertEquals("failed", failureResult1);
			}			

			@Test
			public void testCheck4() {
				//fail("Not yet implemented");
				MoviesInTheatersSingletonService mc = new MoviesInTheatersSingletonService(); 
				String successResult1 = mc.check4("Drama","2019");
				String failureResult1 = mc.check4("Drama", "2016");
				assertEquals("successed", successResult1);
				assertEquals("failed", failureResult1);
			}		
}
