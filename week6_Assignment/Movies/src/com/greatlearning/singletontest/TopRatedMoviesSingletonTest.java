package com.greatlearning.singletontest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.greatlearning.singletonservice.MoviesInTheatersSingletonService;
import com.greatlearning.singletonservice.TopRatedMoviesSingletonService;

public class TopRatedMoviesSingletonTest {
	@Test
	public void testCheck() {
		//fail("Not yet implemented");
		TopRatedMoviesSingletonService mc = new  TopRatedMoviesSingletonService(); 
		String successResult = mc.check("Deva","Comedy");
		String failureResult = mc.check("ugly", "comedy");
		assertEquals("success", successResult);
		assertEquals("failure", failureResult);
	}

	@Test
	public void testCheck1() {
		//fail("Not yet implemented");
		TopRatedMoviesSingletonService mc = new TopRatedMoviesSingletonService(); 
		String successResult1 = mc.check1("Ugly","4");
		String failureResult1 = mc.check1("ugly", "123");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}	


	@Test
	public void testCheck2() {
		//fail("Not yet implemented");
		TopRatedMoviesSingletonService mc = new TopRatedMoviesSingletonService(); 
		String successResult1 = mc.check2("Deva","2014");
		String failureResult1 = mc.check2("Ugly", "2008");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}
	
	@Test
	public void testCheck3() {
		//fail("Not yet implemented");
		TopRatedMoviesSingletonService mc = new TopRatedMoviesSingletonService(); 
		String successResult1 = mc.check3("Comedy","8");
		String failureResult1 = mc.check3("Comedy", "10");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}			

	@Test
	public void testCheck4() {
		//fail("Not yet implemented");
		TopRatedMoviesSingletonService mc = new TopRatedMoviesSingletonService(); 
		String successResult1 = mc.check4("Mystery","4");
		String failureResult1 = mc.check4("Drama", "8");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}		
}
