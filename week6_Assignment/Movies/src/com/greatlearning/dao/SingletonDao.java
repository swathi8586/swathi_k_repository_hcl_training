package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.greatlearning.singletondesignpattern.SingletonDesignPattern;

public class SingletonDao {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection conn =SingletonDesignPattern.getSingletonDesignPatternConnection();
	       try {
	    	 System.out.println("Movies_coming");
	        String p ="select * from movies_coming";
	        Statement statement =conn.createStatement();
	        ResultSet rs =statement.executeQuery(p);
	        while(rs.next()) {
	         System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getInt(3)+" "
	           +rs.getString(4)+" " +rs.getInt(5));
	        }
	        
	        System.out.println(" ");
	        System.out.println("Movies_in_theaters\n");
	        String p1="select * from movies_in_theaters";
	        Statement statement1 =conn.createStatement();
	        ResultSet rs1 =statement1.executeQuery(p1);
	        while(rs1.next()) {
	         System.out.println(rs1.getInt(1)+" "+rs1.getString(2)+" "+
	        		 rs1.getInt(3)+" "+rs1.getString(4) +" " +rs1.getInt(5));
	        }
	        
	        System.out.println(" ");
	        System.out.println("Top_rated_india\n");
	        String p2="select * from top_rated_india";
	        Statement statement2 =conn.createStatement();
	        ResultSet rs2 =statement2.executeQuery(p2);
	        while(rs2 .next()) {
	         System.out.println(rs2 .getString(1)+" "+rs2 .getInt(2)+" "+rs2 .getString(3)+" " +rs2 .getInt(4));
	        }
	        
	        System.out.println(" ");
	        System.out.println("Top_rated_movies\n");
	        String p3="select * from top_rated_movies";
	        Statement statement3 =conn.createStatement();
	        ResultSet rs3 =statement3.executeQuery(p3);
	        while(rs3.next()) {
	         System.out.println(rs3.getString(1)+" "+rs3.getInt(2)+" "+rs3.getString(3)+" " +rs3.getInt(4));
	        }
	       }catch(Exception e) {
	        System.out.println("error");
	       }
	 }

	

	}


