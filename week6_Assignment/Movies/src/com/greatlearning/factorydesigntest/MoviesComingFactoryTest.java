package com.greatlearning.factorydesigntest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.greatlearning.factorydesignservice.MoviesComingFactoryService;
import com.greatlearning.singletonservice.MoviesComingSingletonService;

public class MoviesComingFactoryTest {
	
	@Test
			public void testcheck() {
				//fail("Not yet implemented");
			   MoviesComingFactoryService mc = new MoviesComingFactoryService(); 
				String successResult = mc.check("Lage","Drama");
				String failureResult = mc.check("Dogs", "Comedy");
				assertEquals("success", successResult);
				assertEquals("failure", failureResult);

}
	
	@Test
			public void testCheck1() {
				//fail("Not yet implemented");
				MoviesComingFactoryService mc = new MoviesComingFactoryService(); 
				String successResult1 = mc.check1("Indian","8");
				String failureResult1 = mc.check1("ugly", "123");
				assertEquals("successed", successResult1);
				assertEquals("failed", failureResult1);
			}	
			@Test
					public void testCheck2() {
						//fail("Not yet implemented");
						MoviesComingFactoryService mc = new MoviesComingFactoryService(); 
						String successResult1 = mc.check2("Lage","2006");
						String failureResult1 = mc.check2("Dogs", "2008");
						assertEquals("successed", successResult1);
						assertEquals("failed", failureResult1);
					}	
					@Test
					public void testCheck3() {
						//fail("Not yet implemented");
						MoviesComingFactoryService mc = new MoviesComingFactoryService(); 
						String successResult1 = mc.check3("Comedy","9");
						String failureResult1 = mc.check3("Comedy", "10");
						assertEquals("successed", successResult1);
						assertEquals("failed", failureResult1);
					}			

					@Test
					public void testCheck4() {
						//fail("Not yet implemented");
						MoviesComingFactoryService mc = new MoviesComingFactoryService(); 
						String successResult1 = mc.check4("Drama","2006");
						String failureResult1 = mc.check4("Drama", "2007");
						assertEquals("successed", successResult1);
						assertEquals("failed", failureResult1);
					}		
}
