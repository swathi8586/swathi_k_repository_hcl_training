package com.greatlearning.factorydesigntest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.greatlearning.factorydesignservice.MoviesInThreatersFactoryService;
import com.greatlearning.factorydesignservice.TopRatedIndiaFactoryService;
import com.greatlearning.singletonservice.TopRatedIndiaSingletonService;

public class TopRatedIndiaFactoryTest {
	@Test
	public void testconfirm() {
		//fail("Not yet implemented");
		TopRatedIndiaFactoryService mc = new TopRatedIndiaFactoryService(); 
		String successResult = mc.check("Anbe","Comedy");
		String failureResult = mc.check("Blck", "action");
		assertEquals("success", successResult);
		assertEquals("failure", failureResult);

}
	
	@Test
	public void testCheck1() {
		//fail("Not yet implemented");
		TopRatedIndiaFactoryService mc = new TopRatedIndiaFactoryService(); 
		String successResult1 = mc.check1("Anbe","5");
		String failureResult1 = mc.check1("ugly", "123");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}	
	
	@Test
	public void testCheck2() {
		//fail("Not yet implemented");
		TopRatedIndiaFactoryService mc = new TopRatedIndiaFactoryService(); 
		String successResult1 = mc.check2("Nayakan","1987");
		String failureResult1 = mc.check2("Anbe", "2008");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}
	
	@Test
	public void testCheck3() {
		//fail("Not yet implemented");
		TopRatedIndiaFactoryService mc = new TopRatedIndiaFactoryService(); 
		String successResult1 = mc.check3("Comedy","5");
		String failureResult1 = mc.check3("Comedy", "11");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}			

	@Test
	public void testCheck4() {
		//fail("Not yet implemented");
		TopRatedIndiaFactoryService mc = new TopRatedIndiaFactoryService(); 
		String successResult1 = mc.check4("Thriller","2004");
		String failureResult1 = mc.check4("Drama", "2016");
		assertEquals("successed", successResult1);
		assertEquals("failed", failureResult1);
	}		

}

