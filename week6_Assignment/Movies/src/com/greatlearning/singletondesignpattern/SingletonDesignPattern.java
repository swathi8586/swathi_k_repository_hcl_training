package com.greatlearning.singletondesignpattern;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingletonDesignPattern {
	static Connection con;
	public SingletonDesignPattern() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ Movies_Swathi_k","root","Swathik.123");
			}catch(Exception e) {
			System.out.println("SingletonDesignPattern connection" + e);
		}
	}

	public static Connection getSingletonDesignPatternConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ Movies_Swathi_k","root","Swathik.123");
			return con;
		}catch(Exception e) {
			
		}
		return null;
	}
	
}
	
	


