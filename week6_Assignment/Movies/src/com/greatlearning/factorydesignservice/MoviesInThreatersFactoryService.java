package com.greatlearning.factorydesignservice;

public class MoviesInThreatersFactoryService {
	public String check(String title,String genres) {
		if(title.equals("Panther") && genres.equals("Adventure")) {
			return "success";
		}else {
			return "failure";
		}
	}
	public String check1(String title,String ratings) {
		if(title.equals("Samson") && ratings.equals("8")) {
			return "successed";
		}else {
			return "failed";
		}
	}

	public String check2(String title,String year) {
		if(title.equals("Samson") && year.equals("2019")) {
			return "successed";
		}else {
			return "failed";
		}
	}
	
	
	public String check3(String genres,String ratings ) {
		if(genres.equals("Comedy") && ratings.equals("10")) {
			return "successed";
		}else {
			return "failed";
		}
	}
	
	public String check4(String genres,String year ) {
		if(genres.equals("Drama") && year.equals("2019")) {
			return "successed";
		}else {
			return "failed";
		}
		
		
	
}
}
