package com.greatlearning.factorydesignservice;

public class MoviesComingFactoryService {
	public String check(String title,String genres) {
		if(title.equals("Lage") && genres.equals("Drama")) {
			return "success";
		}else {
			return "failure";
		}
	}

	public String check1(String title,String ratings) {
		if(title.equals("Indian") && ratings.equals("8")) {
			return "successed";
		}else {
			return "failed";
		}
	}
	

	public String check2(String title,String year) {
		if(title.equals("Lage") && year.equals("2006")) {
			return "successed";
		}else {
			return "failed";
		}
	}

	public String check3(String genres,String ratings ) {
		if(genres.equals("Comedy") && ratings.equals("9")) {
			return "successed";
		}else {
			return "failed";
		}
	}
	
	public String check4(String genres,String year ) {
		if(genres.equals("Drama") && year.equals("2006")) {
			return "successed";
		}else {
			return "failed";
		}
	}

}
