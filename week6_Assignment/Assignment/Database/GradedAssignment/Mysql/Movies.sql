Create database  movies_Swathi_k;
use movies_Swathi_k;

create table movies_coming(id int primary key,title varchar(10),year int,genres varchar(10),ratings int);
create table movies_in_theaters(id int primary key,title varchar(10),year int,genres varchar(10),ratings int);
create table top_rated_india(title varchar(10),year int,genres varchar(10),ratings int);
create table top_rated_movies(title varchar(10),year int,genres varchar(10),ratings int);

insert into movies_coming(1,"Sherlock",2018,"Animation",5);
insert into movies_coming(2,"Lage",2006,"Drama",4);
insert into movies_coming(3,"omkara",2005,"Crime",6);
insert into movies_coming(4,"Indian",1996,"Drama",8);
insert into movies_coming(5,"Ankhon",2013,"Comedy",9);
insert into movies_coming(6,"Dog",2018,"Family",7;

insert into  movies_in_theaters(1,"Panther",2018,"Adventure",6);
insert into  movies_in_theaters(2,"Grottman",2017,"Comedy",5);
insert into  movies_in_theaters(3,"Aiyaary",2015,"Crime",4);
insert into  movies_in_theaters(4,"Samson",2019,"Drama",8);
insert into  movies_in_theaters(5,"Loveless",2017,"Drama",9);
insert into  movies_in_theaters(6,"party",2020,"Comedy",10);

insert into  top_rated_india("Anand",1971,"Drama",5);
insert into  top_rated_india("Dangal",2016,"Action",9);
insert into  top_rated_india("Drishyam",2013,"Thriller",10);
insert into  top_rated_india("Nayakan",1987,"Crime",6);
insert into  top_rated_india("Anbe",2003,"Comedy",5);
insert into  top_rated_india("Black",2004,"Thriller",7);

insert into  top_rated_movies("maqbool",2003,"Crime",6);
insert into  top_rated_movies("premam",2015,"Drama",7);
insert into  top_rated_movies("Deva",2014,"Comedy",9);
insert into  top_rated_movies("Barfi",2012,"Comedy",8);
insert into  top_rated_movies("ugly",2013,"Mystery",4);
insert into  top_rated_movies("Legend",2002,"Drama",9);

select * from movies_coming;

select * from movies_in_theaters;

select * from top_rated_india;

select * from top_rated_movies;








